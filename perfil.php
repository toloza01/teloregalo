<?php
include('bd/conexion.php');

$rut = $_GET['id_user'];

$user = $conexion->query("SELECT * FROM usuarios WHERE rut = '$rut'");
$num  = $user->num_rows;
if ($num == 0) {
  header("location: index.php");
}
$userF = $user->fetch_assoc();
$comunaQ = "SELECT nombre FROM comuna WHERE id_comuna='$userF[id_comuna]'";
$query = $conexion->query($comunaQ);
$comuna = $query->fetch_assoc();

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <title>Te lo Regalo</title>
  <link href="css/styles.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
</head>

<body class="sb-nav-fixed">
  <?php include("navbar.php"); ?>
  <div id="layoutSidenav">
    <div id="layoutSidenav_nav">
      <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
          <div class="nav">
            <?php if (isset($_SESSION['admin'])) { ?>
              <div class="sb-sidenav-menu-heading text-white">Admin</div>
              <a class="nav-link" href="admin.php">
                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                Administracion
              </a>
            <?php  }  ?>
            <div class="sb-sidenav-menu-heading text-white">Apoya a la comunidad</div>
            <a class="nav-link active" href="newPost.php">
              <div class="sb-nav-link-icon"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z" />
                  <path fill-rule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z" />
                  <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                </svg></div>
              Nueva publicacion
            </a>
            <?php if(isset($_SESSION['id_user'])){ ?>  
                        <a href="interesados.php" class="nav-link active">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-users"></i>
                            </div>
                            Mis interesados
                        </a>
                    <?php } ?>
          </div>
        </div>
        <div class="sb-sidenav-footer">
          <div class="small">Logeado como:</div>
          <div class="text-white">
            <?php
            if (isset($_SESSION['user'])) {
              $nombre = strstr($_SESSION['user'], ' ', true);
              echo $_SESSION['user'];
            } else {
              echo "Invitado";
            }
            ?>
          </div>
        </div>
      </nav>
    </div>
    <div id="layoutSidenav_content">
      <!-----CONTENIDO AQUIIIIIIII -------------------------->
      <?php if (isset($userF)) { ?>
        <div class="container-fluid">
          <div class="col-md-10 offset-1 mt-2">
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Nombre: <?= $userF['nombre'] ?></h3>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-md-5 col-lg-5"> <img alt="User Pic" src="https://cdn4.iconfinder.com/data/icons/avatars-xmas-giveaway/128/batman_hero_avatar_comics-512.png" width="300px" height="300px" class="img-circle img-responsive"> </div>
                  <div class=" col-md-7 col-lg-7 ">
                    <table class="table table-user-information">
                      <tbody>
                        <tr>
                          <td>RUT:</td>
                          <td><?= $userF['rut'] ?></td>
                        </tr>
                        <tr>
                          <td>Email:</td>
                          <td><?= $userF['email'] ?></td>
                        </tr>
                        <tr>
                          <td>Telefono:</td>
                          <td><?= $userF['telefono'] ?></td>
                        </tr>
                        <tr>
                          <td>Direccion</td>
                          <td><?= $userF['direccion'] ?></td>
                        </tr>
                        <tr>
                          <td>Genero</td>
                          <td><?= $userF['sexo'] ?></td>
                        </tr>
                        <tr>
                          <td>Comuna</td>
                          <td><?= $comuna['nombre'] ?></td>
                        </tr>
                        <tr>
                          <td>Fecha de nacimiento</td>
                          <td><?= $userF['nacimiento'] ?></td>
                        </tr>

                      </tbody>
                    </table>
                    <div class="row">
                      <?php if (isset($_SESSION['user']) && $_SESSION['id_user'] == $userF['rut']) { ?>
                        <div class="col-md-6"><a href="editar.php?rut=<?php echo $userF['rut'] ?>" class="btn btn-primary btn-block">Editar Perfil</a></div>
                        <div class="col-md-6"> <a href="bd/eliminar.php?rut=<?php echo $userF['rut'] ?>" class="btn btn-danger btn-block">Eliminar usuario</a></div>
                      <?php } ?>

                    </div>

                  </div>
                </div>
              </div>
              <div class="card-footer">
                <a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                <span class="pull-right">
                  <a data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                  <a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                </span>
              </div>

            </div>
          </div>

          <!-----/ FIIIIN    CONTENIDO -------------------------->
        </div>

      <?php } ?>
      <?php include("footer.php"); ?>
    </div>


    <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/chart-area-demo.js"></script>
    <script src="assets/demo/chart-bar-demo.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/datatables-demo.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

</body>

</html>