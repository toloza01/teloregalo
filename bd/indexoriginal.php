<?php
header('Content-Type: text/html; charset=UTF-8');

include('bd/conexion.php');
if (isset($_SESSION['user'])) {
    $rut = $_SESSION['id_user'];
    $admin = "SELECT * FROM rol_user WHERE rut_user='$rut' AND id_rol=1";
    $adminQ = mysqli_query($conexion, $admin);
    $adminr = mysqli_fetch_row($adminQ);
}
$sql_usuarios = "SELECT * FROM users";
$consulta_usuarios = $conexion->query($sql_usuarios);
$sql_posts = "SELECT * FROM posts ORDER BY fecha DESC";
$posts = $conexion->query($sql_posts);
$query = "SELECT id_region,nombre FROM region ORDER BY nombre ASC";
$regiones =  $conexion->query($query);
$regiones2 =  $conexion->query($query);
$categorias = $conexion->query("SELECT * FROM categorias");
$categorias2 = $conexion->query("SELECT * FROM categorias");

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Home</title>
    <link href="css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.0/css/bootstrap-slider.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.0/bootstrap-slider.min.js"></script>
</head>

<body class="sb-nav-fixed">
    <?php include("navbar.php"); ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <?php if (isset($_SESSION['admin'])) { ?>
                            <div class="sb-sidenav-menu-heading text-white">Admin</div>
                            <a class="nav-link" href="admin.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Administracion
                            </a>
                        <?php  }  ?>
                        <div class="sb-sidenav-menu-heading text-white">Tienes un proyecto?</div>
                        <a class="nav-link" href="index.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                            Publicar Aviso
                        </a>
                        <!----
                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
                                <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                                Pages
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth">
                                        Authentication
                                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                    </a>
                                    <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                        <nav class="sb-sidenav-menu-nested nav">
                                            <a class="nav-link" href="login.html">Login</a>
                                            <a class="nav-link" href="register.html">Register</a>
                                            <a class="nav-link" href="password.html">Forgot Password</a>
                                        </nav>
                                    </div>
                                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseError" aria-expanded="false" aria-controls="pagesCollapseError">
                                        Error
                                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                    </a>
                                    <div class="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                        <nav class="sb-sidenav-menu-nested nav">
                                            <a class="nav-link" href="401.html">401 Page</a>
                                            <a class="nav-link" href="404.html">404 Page</a>
                                            <a class="nav-link" href="500.html">500 Page</a>
                                        </nav>
                                    </div>
                                </nav>
                            </div>
                        -->
                        <div class="sb-sidenav-menu-heading text-white">Apoya a la comunidad</div>
                        <a class="nav-link" href="index.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                            Publicar articulo
                        </a>
                    </div>
                </div>

                <div class="sb-sidenav-footer">
                    <div class="small">Logeado como:</div>
                    <div class="text-white">
                        <?php
                        if (isset($_SESSION['user'])) {
                            $nombre = strstr($_SESSION['user'], ' ', true);
                            echo $_SESSION['user'];
                        } else {
                            echo "Invitado";
                        }
                        ?>
                    </div>
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <!-----CONTENIDO AQUIIIIIIII -------------------------->
            <!---- AGREGAR PUBLICACION -->
            <div class="container">
                <div class="row mt-3">
                    <div class="col-md-4">
                        <!---FILTROS---->
                        <div class="card">
                            <div class="card-header text-center">
                                <h4>Filtros</h4>
                            </div>
                            <div class="card-body">
                                <form id="form-buscador" method="post">
                                    <div class="form-group">
                                        <label for="buscador">Buscador</label>
                                        <div class="input-group mb-3">
                                            <input type="text" id="buscador" name="busqueda" class="form-control" placeholder="Ingrese su busqueda" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" type="submit" id="btn-buscador"><i class="fas fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <form id="form-filtros" method="post">
                                    <div class="form-group">
                                        <label for="fecha">Fecha de publicacion</label>
                                        <select name="fecha" class="form-control" id="fecha">
                                            <option value="1">Ultimas 24 horas</option>
                                            <option value="7">Ultimos 7 dias</option>
                                            <option value="30">Ultimo Mes</option>
                                            <option value="365">Ultimo año</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="region">Region</label>
                                        <select name="regiones" id="regiones" class="form-control">
                                            <option value="0">Seleccionar Region</option>
                                            <?php while ($row = $regiones->fetch_assoc()) { ?>
                                                <option value="<?php echo $row['id_region']; ?>">
                                                    <?php echo $row['nombre']; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="ciudades">Provincia</label>
                                        <select name="ciudades" id="ciudades" class="form-control">
                                            <option value="0">Seleccionar Provincia</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="comunas">Comuna</label>
                                        <select name="comunas" id="comunas" class="form-control">
                                            <option value="0">Seleccionar Comuna</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="categoria">Categorias</label>
                                        <select class="form-control" required name="categoria2" id="categoria2">
                                            <option style="background-color:white;" value="0">Escoge una categoria</option>
                                            <?php while ($categoriaP2 = $categorias2->fetch_assoc()) { ?>
                                                <option style="background-color:#dcdcc3; font-weight: bold;" value="<?php echo $categoriaP2['id_categoria']; ?>">
                                                    <?php echo $categoriaP2['nombre']; ?></b>
                                                </option>
                                                <?php $categoriaHQ2 = $conexion->query("SELECT * FROM subcategorias WHERE id_categoria=$categoriaP2[id_categoria]"); ?>
                                                <?php foreach ($categoriaP2 as $categoriaP2) {
                                                    while ($categoriaH2 = $categoriaHQ2->fetch_assoc()) { ?>
                                                        <option value="<?php echo $categoriaH2['id_subcategoria']; ?>">
                                                            <?php echo $categoriaH2['nombre']; ?></b>
                                                        </option>

                                                    <?php  } ?>
                                                <?php } ?>
                                            <?php } ?>

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="tipo">Tipo de publicacion</label>
                                        <select class="form-control" name="tipo" id="tipo">
                                            <option value="regalar">Regalar</option>
                                            <option value="pedir">Solicitar</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" id="btn-filtrar" class="btn btn-success btn-block btn-lg">Filtrar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!----FIN FILTROS------->
                    </div>
                    <div class="col-md-8">
                        <?php if (isset($_SESSION['user'])) {   ?>
                            <div class="card">
                                <div class="card-header bg-primary text-center text-white">
                                    <h4>Publicar aviso</h4>
                                </div>
                                <div class="card-body">
                                    <?php if (isset($_SESSION['error'])) { ?>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <?php echo $_SESSION['error']; ?>
                                            <?php session_unset(); ?>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <?php } ?>
                                    <form action="bd/guardarPost.php" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label for="titulo">Titulo de publicacion</label>
                                            <input type="text" class="form-control" max="250" required placeholder="Me falta un xxxx para mi proyecto!" id="titulo" name="titulo">
                                        </div>
                                        <div class="form-group">
                                            <label for="contenido">Contenido</label>
                                            <textarea name="contenido" id="contenido" required class="form-control" placeholder="Necesito algo para mi proyecto URGENTE!" style="min-height:100px; max-height:300px;" rows="3"></textarea>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="regiones2">Region</label>
                                                    <select name="regiones2" id="regiones2" required class="form-control">
                                                        <option value="0">Seleccionar Region</option>
                                                        <?php while ($row2 = $regiones2->fetch_assoc()) { ?>
                                                            <option value="<?php echo $row2['id_region']; ?>">
                                                                <?php echo $row2['nombre']; ?>
                                                            </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="ciudades2">Provincia</label>
                                                    <select name="ciudades2" id="ciudades2" required class="form-control">
                                                        <option value="0">Seleccionar Provincia</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="comunas2">Comuna</label>
                                                    <select name="comunas2" id="comunas2" required class="form-control">
                                                        <option value="0">Seleccionar Comuna</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group mb-3">
                                                    <div class="custom-file">
                                                        <input type="file" name="imagen" class="custom-file-input" id="inputGroupFile02" />
                                                        <label class="custom-file-label" for="inputGroupFile02">Agregar Imagen</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="form-control" required name="categoria" id="categoria">
                                                        <option style="background-color:white;" value="0" disabled>Escoge una categoria</option>
                                                        <?php while ($categoriaP = $categorias->fetch_assoc()) { ?>
                                                            <option style="background-color:#dcdcc3; font-weight: bold;" value="<?php echo $categoriaP['id_categoria']; ?>">
                                                                <?php echo $categoriaP['nombre']; ?></b>
                                                            </option>
                                                            <?php $categoriaHQ = $conexion->query("SELECT * FROM subcategorias WHERE id_categoria=$categoriaP[id_categoria]"); ?>
                                                            <?php foreach ($categoriaP as $categoriaP) {
                                                                while ($categoriaH = $categoriaHQ->fetch_assoc()) { ?>
                                                                    <option value="<?php echo $categoriaH['id_subcategoria']; ?>">
                                                                        <?php echo $categoriaH['nombre']; ?></b>
                                                                    </option>

                                                                <?php  } ?>


                                                            <?php } ?>
                                                        <?php } ?>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" name="publicar" id="publicar" class="btn btn-success btn-block">Publicar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        <?php } ?>
                        <!----FIN AGREGAR PUBLICACION---->
                        <!--- LISTADO DE POSTS----->
                        <?php
                        if ($posts->num_rows > 0) {
                        ?>
                            <div class="lista">
                                <?php
                                while ($row = $posts->fetch_assoc()) {
                                    $nombreq = "SELECT nombre FROM usuarios WHERE rut='$row[rut_user]'";
                                    $user = mysqli_query($conexion, $nombreq);
                                    $nombre = mysqli_fetch_row($user);
                                    $rut = "SELECT rut FROM usuarios  WHERE rut='$row[rut_user]'";
                                    $rutq = mysqli_query($conexion, $rut);
                                    $rutUser = mysqli_fetch_row($rutq);
                                    $comunaQ = mysqli_query($conexion, "SELECT nombre FROM comuna WHERE id_comuna='$row[id_comuna]'");
                                    $comuna = mysqli_fetch_row($comunaQ);
                                    $categoriaQ = mysqli_query($conexion, "SELECT categorias.nombre FROM categorias,categorizar WHERE categorias.id_categoria=categorizar.id_categoria 
                                    AND categorizar.id_post='$row[id_post]'");
                                    $categoriaF = mysqli_fetch_assoc($categoriaQ);
                                ?>
                                    <!-- POST ---->
                                    <div class="row">
                                        <div class="col-md-12 mt-3">
                                            <div id="post">
                                                <div class="card">
                                                    <div class="card-header bg-danger text-white">
                                                        <div class="row">
                                                            <div class="col-md-4 offset-4 text-center">
                                                                <h3><?php echo $row['titulo'] ?></h3>
                                                            </div>
                                                         
                                                        </div>
                                                    </div>
                                                    <div class="card-body">
                                                        <?php echo $row['contenido'] ?>
                                                        <br>
                                                        <br>
                                                        <?php if (isset($row['imagen']) && $row['imagen'] != null) { ?>
                                                            <div class="card">
                                                                <img height="400px" src="img/uploads/<?= $row['imagen'] ?>" alt="">
                                                            </div>
                                                        <?php } else {
                                                            echo " Imagen NOT FOUND :C <br>";
                                                            echo "<hr>";
                                                        } ?>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="row mt-3 ml-2">
                                                                    <div class="col-md-5">
                                                                        <div class="row">
                                                                            <i class="fas fa-street-view mr-2" style="font-size: 30px;"></i>
                                                                            <h4><?php echo $comuna[0] ?></h4>
                                                                        </div>
                                                                        <div class="row">
                                                                            <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-tag-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                <path fill-rule="evenodd" d="M2 1a1 1 0 0 0-1 1v4.586a1 1 0 0 0 .293.707l7 7a1 1 0 0 0 1.414 0l4.586-4.586a1 1 0 0 0 0-1.414l-7-7A1 1 0 0 0 6.586 1H2zm4 3.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                                                                            </svg>
                                                                            <?php echo  $categoriaF['nombre'] ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2 offset-5">
                                                                        <div class="row">
                                                                        <?php if ((isset($_SESSION['id_user']) && $row['rut_user'] == $_SESSION['id_user']) || isset($_SESSION['admin'])) {  ?>
                                                                            <a class="btn btn-danger btn-md mr-1" title="Eliminar Post" href="bd/eliminarPub.php?id_post=<?php echo $row['id_post'] ?>" name="eliminarPub" id="eliminarPub"><i class="fa fa-2x fa-trash-alt "></i></a>
                                                                        <?php } ?>
                                                                        <a href="post.php?id=<?= $row['id_post'] ?>" title="Ver comentarios" class="btn btn-success btn-lg">
                                                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-chat-right-text" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                                <path fill-rule="evenodd" d="M2 1h12a1 1 0 0 1 1 1v11.586l-2-2A2 2 0 0 0 11.586 11H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1zm12-1a2 2 0 0 1 2 2v12.793a.5.5 0 0 1-.854.353l-2.853-2.853a1 1 0 0 0-.707-.293H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12z" />
                                                                                <path fill-rule="evenodd" d="M3 3.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3 6a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9A.5.5 0 0 1 3 6zm0 2.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z" />
                                                                            </svg>
                                                                        </a>
                                                                        </div>
                                                                     
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="card-footer">
                                                        <div class="row">
                                                            <div class="col-md-6 left">
                                                                <p>Publicado por
                                                                    <?php if (isset($_SESSION['id_user']) && $row['rut_user'] == $_SESSION['id_user']) { ?>
                                                                        Mi
                                                                    <?php } else {  ?>
                                                                        <a href="perfil.php?id_user=<?php echo $rutUser[0] ?>"> <?php echo $nombre[0]  ?> </a>
                                                                    <?php } ?>
                                                            </div>
                                                            <div class="col-md-3 offset-3">
                                                                <?php echo $row['fecha'] ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr>
                                        </div>
                                    </div>
                                    <!-- FIN POST ---->

                                <?php
                                }
                                ?>
                            </div>

                            <div class="cargando" style="display: none;">
                                    <div class="row">
                                        <div class="col-md-6 offset-5 mt-5">
                                         <img src="img/loading.gif" alt="Cargando">
                                        </div>
                                    </div>
                            </div>
                            <div class="filtrados">
                                
                            </div>
                        <?php } else { ?>


                            <div class="row">
                                <div class="col-md-12 col-sm-12 mt-3">
                                    <div class="col-md-12 col-sm-4">
                                        <div class="card">
                                            <div class="text-center">
                                                <H1>NO HAY POSTS PARA MOSTRAR :C</H1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php  } ?>

                        <!--- FIN LISTADO DE POSTS----->

                    </div>
                </div>
            </div>
            <!-----/ FIIIIN    CONTENIDO -------------------------->
            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class="text-muted">Copyright &copy; te lo regalo 2020

                        </div>

                    </div>
                </div>
            </footer>
        </div>


    </div>


</body>

</html>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="js/scripts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
<script src="assets/demo/chart-area-demo.js"></script>
<script src="assets/demo/chart-bar-demo.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="assets/demo/datatables-demo.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="js/filtros.js"></script>