<?php
include("conexion.php");

if (isset($_POST['update'])) {

   if (
      !empty($_POST['titulo']) && !empty($_POST['contenido'])
      && !empty($_POST['regiones2']) && !empty($_POST['ciudades2'])  && !empty($_POST['comunas2'])
      && !empty($_POST['categorias']) && !empty($_POST['tipo']) && !empty($_POST['estado'])
   ) {
      //recoger datos de post
      $titulo = $_POST['titulo'];
      $contenido = $_POST['contenido'];
      $comuna = $_POST['comunas2'];
      $rut_user = $_SESSION['id_user'];
      $tipo = $_POST['tipo'];
      $estado_producto = $_POST['estado'];
      //estado entrega por defecto de la publicacion disponible
      $estado_entrega = 1;
      //limite de texto descripcion
      if (strlen($contenido) > 250) {
         session_start();
         $_SESSION['error']="La descripcion es muy larga!";
         header("location: ../index.php");
         die();
      }
      // Recibo los datos de la imagen

      $nombre_img = $_FILES['imagen']['name'];
      $tipoimg = $_FILES['imagen']['type'];
      $tamano = $_FILES['imagen']['size'];
      $ruta = $_FILES['imagen']['tmp_name'];
      //Si existe imagen y tiene un tamaño correcto
      if ($nombre_img == !NULL || $nombre_img!="") {
         //indicamos los formatos que permitimos subir a nuestro servidor
         if (($_FILES["imagen"]["type"] == "image/gif")
            || ($_FILES["imagen"]["type"] == "image/jpeg")
            || ($_FILES["imagen"]["type"] == "image/jpg")
            || ($_FILES["imagen"]["type"] == "image/png")
         ) {
            // Ruta donde se guardarán las imágenes que subamos
            $destino = "../img/uploads/" . $nombre_img;
            copy($ruta, $destino);

            //$directorio = $_SERVER['DOCUMENT_ROOT'].'/img/uploads/';
            // Muevo la imagen desde el directorio temporal a nuestra ruta indicada anteriormente
            //move_uploaded_file($_FILES['imagen']['tmp_name'],$destino);
         } else {
            //si no cumple con el formato
            session_start();
            $_SESSION['error']="La imagen debe ser de tipo jpg,jpeg,png o gif!";
            header("location: ../index.php");
            die();
         }
      } else {
         session_start();
         //si existe la variable pero se pasa del tamaño permitido
         $_SESSION['error']="No haz seleccionado ninguna imagen!";
         header("location: ../index.php");
         die();
      }
      //Sacar fecha actual
      date_default_timezone_set("America/Santiago");
      $fecha = date('Y-m-d H:i:s', time());
      //valor fecha para comparar
      $valor_fecha = strtotime($fecha);
      //Insertar post
      $query= "UPDATE posts SET titulo='$_POST[titulo]', contenido='$_POST[contenido]', fecha='$fecha', rut_user='$rut_user', id_estado='$_POST[estado]', id_entrega=1, id_comuna='$_POST[comunas2]', tipo_anuncio='$_POST[tipo]' WHERE id_post='$_GET[post]'";
    
      $post = mysqli_query($conexion, $query);
      if(!$post){
         session_start();
         $_SESSION['error']="Error al actualizar su post!";
         header("location: ../index.php");
         die();
      }
      //sacar id para insertar categoria
      $id_post = mysqli_query($conexion, "SELECT id_post FROM posts WHERE fecha='$fecha' AND rut_user='$rut_user'");
      $postF = mysqli_fetch_assoc($id_post);
      $query_img=mysqli_query($conexion,"UPDATE posts SET imagen='$nombre_img' WHERE id_post='$postF[id_post]'");
      if (isset($_POST['categorias']) && $post) {
         $categoria = $_POST['categorias'];
         $num_category = count($categoria);
         if ($num_category > 0) {
            $categorias = implode(',', $categoria);
            $borrar = $conexion->query("DELETE FROM categorizar WHERE id_post='$postF[id_post]'");
            foreach ($categoria as $categoria) {
               $query_cat = $conexion->query("INSERT INTO categorizar(id_post,id_categoria) VALUES('$postF[id_post]','$categoria')");
            }
         }
      } else {
         session_start();
         $_SESSION['error']="Error al intentar insertar la publicacion!";
         header("location: ../index.php");
         
      }
      if ($query_cat && $borrar && $query_img && $post) {
         session_start();
         $_SESSION['success']="Publicacion actualizada con exito!";
         header("location: ../index.php");
      } else {
        session_start();
        $_SESSION['error']="Error al actualizar su post!";
        header("location: ../index.php");
      }
   } else {
      session_start();
      $_SESSION['error']="Error! No pueden haber campos vacios en el formulario!";
         header("location: ../index.php");
   }
}
