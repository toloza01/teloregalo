<?php
    require("conexion.php");
    if(isset($_SESSION['user'])){
        $id_post= $_GET['id_post'];
        $query = mysqli_query($conexion,"SELECT rut_user FROM posts WHERE id_post='$id_post'");
        $row = mysqli_fetch_row($query);
    }
    date_default_timezone_set("America/Santiago");
    $fecha = date('Y-m-d H:i:s', time());
?>

<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Reportar post - Teloregalo</title>
    <link href="../css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.0/css/bootstrap-slider.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.0/bootstrap-slider.min.js"></script>
</head>

<body class="sb-nav-fixed">
<?php include("../navbar2.php"); ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <?php if (isset($_SESSION['admin'])) { ?>
                            <div class="sb-sidenav-menu-heading text-white">Admin</div>
                            <a class="nav-link" href="../admin.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Administracion
                            </a>
                        <?php  }  ?>
                        <div class="sb-sidenav-menu-heading text-white">Apoya a la comunidad</div>
                        <a class="nav-link active" href="../newPost.php">
                            <div class="sb-nav-link-icon"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z" />
                                    <path fill-rule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z" />
                                    <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                </svg></div>
                            Nueva publicacion
                        </a>
                    </div>
                </div>

                <div class="sb-sidenav-footer">
                    <div class="small">Logeado como:</div>
                    <div class="text-white">
                        <?php
                        if (isset($_SESSION['user'])) {
                            $nombre = strstr($_SESSION['user'], ' ', true);
                            echo $_SESSION['user'];
                        } else {
                            echo "Invitado";
                        }
                        ?>
                    </div>
                </div>

            </nav>
        </div>
        <div id="layoutSidenav_content">
            <!-----CONTENIDO AQUIIIIIIII -------------------------->
            <!---- REPORTAR PUBLICACION -->
            <div class="container">
                <div class="row">
                    <div class="col-md-12 mt-2">
                        <div class="col-md-8 offset-2">
                            <div class="card">
                                <div class="card-header text-center bg-danger text-white">
                                    <h4>Reportar publicacion</h4>
                                </div>
                                <div class="card-body">
                                    <form action="reportado.php?post=<?php echo $id_post?>" method="post">
                                        <div class="form-group">
                                            <label for="motivo">Motivo</label>
                                            <select name="motivo" id="motivo" class="form-control">
                                                <option value="Sin relacion">Sin relacion a la pagina</option>
                                                <option value="Publicacion falsa">Publicacion falsa</option>
                                                <option value="Publicacion ofensiva">Publicacion ofensiva</option>
                                                <option value="Otro">Otro</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="fecha_reporte">Fecha reporte:</label>
                                            <input type="text" class="form-control" name="fecha_reporte" value="<?php echo $fecha ?>" readonly="readonly" >
                                        </div>
                                        <div class="form-group">
                                            <label for="comentario">Comentario:</label>
                                            <textarea name="comentario" style="max-height: 200px; min-height: 100px;" id="comentario" class="form-control" cols="30" rows="5"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" name="reportado" class="btn btn-block btn-danger btn-md ">Reportar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-----/ FIIIIN  CONTENIDO ----------------------->
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; te lo regalo 2020</div>
                </div>
            </div>
        </footer>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="../js/scripts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/chart-area-demo.js"></script>
    <script src="assets/demo/chart-bar-demo.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/datatables-demo.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

</body>

</html>