<?php
    require("conexion.php");
    if(isset($_SESSION['user'])){
        $id_post= $_GET['id_post'];
        $query2 = mysqli_query($conexion,"SELECT rut_user FROM posts WHERE id_post='$id_post'");
        $row = mysqli_fetch_row($query2);
        $postq = mysqli_query($conexion,"SELECT * FROM posts WHERE id_post='$id_post'");
        $postf = $postq->fetch_assoc();
    }

    date_default_timezone_set("America/Santiago");
    $fecha = date('Y-m-d H:i:s', time());

    $sql_usuarios = "SELECT * FROM users";
    $consulta_usuarios = $conexion->query($sql_usuarios);
    $sql_posts = "SELECT * FROM posts ORDER BY fecha DESC";
    $posts = $conexion->query($sql_posts);
    $query = "SELECT id_region,nombre FROM region ORDER BY nombre ASC";
    $regiones =  $conexion->query($query);
    $regiones2 =  $conexion->query($query);
    $categorias = $conexion->query("SELECT * FROM categorias");
    $categorias2 = $conexion->query("SELECT * FROM categorias");
?>

<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Editar Post- Teloregalo</title>
    <link href="../css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.0/css/bootstrap-slider.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.0/bootstrap-slider.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
</head>

<body class="sb-nav-fixed">
<?php include("../navbar2.php"); ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <?php if (isset($_SESSION['admin'])) { ?>
                            <div class="sb-sidenav-menu-heading text-white">Admin</div>
                            <a class="nav-link" href="../admin.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Administracion
                            </a>
                            <?php  }  ?>
                            <div class="sb-sidenav-menu-heading text-white">Apoya a la comunidad</div>
                            <a class="nav-link active" href="../newPost.php">
                                <div class="sb-nav-link-icon"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z" />
                                    <path fill-rule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z" />
                                    <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                </svg></div>
                            Nueva publicacion
                        </a>
                    </div>
                </div>

                <div class="sb-sidenav-footer">
                    <div class="small">Logeado como:</div>
                    <div class="text-white">
                        <?php
                        if (isset($_SESSION['user'])) {
                            $nombre = strstr($_SESSION['user'], ' ', true);
                            echo $_SESSION['user'];
                        } else {
                            echo "Invitado";
                        }
                        ?>
                    </div>
                </div>

            </nav>
        </div>
        <div id="layoutSidenav_content">
            <!-----CONTENIDO AQUIIIIIIII -------------------------->
            <!---- REPORTAR PUBLICACION -->
            <div class="container mt-3">
                <div class="row">
                    <div class="col-md-8 offset-2">
                        <div class="card shadow-lg">
                            <div class="card-header text-center text-white bg-primary">
                                <h3>Editar Publicacion</h3>
                            </div>
                            <form action="updatePost.php?post=<?php echo $postf['id_post']?>" method="post" enctype="multipart/form-data">
                                <div class="page1">
                                    <div class="card-body">
                                        
                                        <div class="form-group">
                                            <label for="titulo">Titulo*</label>
                                            <input type="text" value="<?php echo $postf['titulo']?>" required class="form-control" name="titulo" placeholder="Cree un titulo para su publicacion">
                                        </div>
                                        <div class="form-group">
                                            <label for="descripcion">Descripcion*</label>
                                            <textarea class="form-control" name="contenido" id="" cols="30" rows="5" style="min-height: 100px; max-height: 200px; resize:vertical; overflow:auto;"><?php echo $postf['contenido']?> </textarea>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="imagen">Cambiar Imagen</label>
                                                    <input id="inputFile1" value="<?php echo $postf['imagen']?>" name="imagen" type="file" class="form-control">
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="imageref card" >
                                                        <img src="../img/uploads/<?=$postf['imagen']?>" width="100%" height="200px" height="100px" id="img1">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group card">
                                            <div class="container mt-2">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="regiones">Region*</label>
                                                            <select name="regiones2" required id="regiones2" class="form-control">
                                                                <option value="0">Seleccionar Region</option>
                                                                <?php while ($row = $regiones->fetch_assoc()) { ?>
                                                                    <option value="<?php echo $row['id_region']; ?>">
                                                                        <?php echo $row['nombre']; ?>
                                                                    </option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="ciudades">Provincia*</label>
                                                            <select name="ciudades2" required id="ciudades2" class="form-control">
                                                                <option value="0">Seleccionar Provincia</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="comunas">Comuna*</label>
                                                            <select name="comunas2" required id="comunas2" class="form-control">
                                                                <option value="0">Seleccionar Comuna</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="page2" style="display: none;">
                                    <div class="card-body">
                                        <div class="form-group">
                                        
                                            <label for="marcas">Seleccione hasta 3 categorias*</label>
                                            <select required class='form-control mi-selector' style="width: 100%;" id="categorias" name='categorias[]' multiple='multiple'>
                                                <option value='0' disabled>Seleccionar una categoria</option>
                                                <?php while ($categoria = $categorias->fetch_assoc()) { ?>
                                                    <option value="<?= $categoria['id_categoria'] ?>"><?= $categoria['nombre'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="tipo">Tipo de anuncio*</label>
                                            <select class="form-control" value=<?php echo $postf['tipo_anuncio']?> name="tipo" id="tipo">
                                                <?php if($postf['tipo_anuncio']=="Regala"){ ?>
                                                    <option selected value="Regala">Regala</option>
                                                    <option value="Busca">Busca</option>
                                                <?php }else { ?>
                                                    <option value="Regala">Regala</option>
                                                    <option selected value="Busca">Busca</option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="estado">Estado del producto</label>
                                            <select class="form-control" name="estado" id="estado">
                                                <?php if($postf['id_estado']==1){ ?>
                                                    <option selected value="1">Nuevo</option>
                                                    <option value="2">Usado</option>
                                                <?php }else { ?>
                                                    <option value="1">Nuevo</option>
                                                    <option selected value="2">Usado</option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6 offset-3">
                                                <button class="btn btn-success btn-block btn-lg" name="update" type="submit">Actualizar</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-4">
                                        <button id="preview-page" class="btn btn-success">Pagina anterior</button>
                                    </div>
                                    <div class="col-md-3 offset-5">
                                        <button id="next-page" class="btn btn-success">Siguiente pagina</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-----/ FIIIIN  CONTENIDO ----------------------->
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy; te lo regalo 2020</div>
                </div>
            </div>
        </footer>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="../js/scripts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/chart-area-demo.js"></script>
    <script src="assets/demo/chart-bar-demo.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/datatables-demo.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="../js/filtros2.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        function init() {

            var inputFile = document.getElementById('inputFile1');

            inputFile.addEventListener('change', mostrarImagen, false);
        }

        function mostrarImagen(event) {
            var file = event.target.files[0];
            var reader = new FileReader();
            reader.onload = function(event) {
                var img = document.getElementById('img1');
                var cont_img = $('.imageref');
                img.src = event.target.result;
                cont_img.css("display", "block");

            }
            reader.readAsDataURL(file);
        }

        window.addEventListener('load', init, false);

        $(document).ready(function() {
            $('.mi-selector').select2({
                maximumSelectionLength: 3,
                language: "es",
                minimumSelectionLength: 1,
            });

            $('#preview-page').hide();

            $('#next-page').click(function() {
                $('#preview-page').show();
                $('.page1').hide();
                $('.page2').show('fast');
                $('#next-page').hide();

            });
            $('#preview-page').click(function() {
                $('#next-page').show();
                $('.page2').hide();
                $('.page1').show('fast');
                $('#preview-page').hide();
            });
        });
    </script>

</body>

</html>