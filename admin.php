<?php
include('bd/conexion.php');

if (!isset($_SESSION['admin'])) {
    header("location: index.php");
}

$sql_usuarios = "SELECT * FROM users";
$consulta_usuarios = $conexion->query($sql_usuarios);
$sql_posts = "SELECT * FROM posts";
$posts = $conexion->query($sql_posts);

$queryusers = "SELECT COUNT(*) totalusers FROM usuarios";
$query = mysqli_query($conexion, $queryusers);
$fila = mysqli_fetch_assoc($query);

$querypub = "SELECT COUNT(*) totalpub FROM posts";
$consulta_pub = mysqli_query($conexion, $querypub);
$fila2 = mysqli_fetch_assoc($consulta_pub);

$queryreps = "SELECT COUNT(*) totalreps FROM reportes";
$consulta_reps = mysqli_query($conexion, $queryreps);
$fila3 = mysqli_fetch_assoc($consulta_reps);

$querycat = "SELECT COUNT(*) totalcat FROM categorias";
$consulta_cat = mysqli_query($conexion, $querycat);
$fila4 = mysqli_fetch_assoc($consulta_cat);


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Administración - Teloregalo</title>
    <link href="css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
</head>

<body class="sb-nav-fixed">
    <?php include("navbar.php") ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
            <div class="sb-sidenav-menu">
                    <div class="nav">
                        <?php if (isset($_SESSION['admin'])) { ?>
                            <div class="sb-sidenav-menu-heading text-white">Admin</div>
                            <a class="nav-link" href="admin.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Administracion
                            </a>
                        <?php  }  ?>                       
                        <div class="sb-sidenav-menu-heading text-white">Apoya a la comunidad</div>
                        <a class="nav-link active" href="newPost.php">
                            <div class="sb-nav-link-icon"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z" />
                                    <path fill-rule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z" />
                                    <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                </svg></div>
                            Nueva publicacion
                        </a>
                        <?php if(isset($_SESSION['id_user'])){ ?>  
                        <a href="interesados.php" class="nav-link active">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-users"></i>
                            </div>
                            Mis interesados
                        </a>
                    <?php } ?>
                    </div>
                </div>

                <div class="sb-sidenav-footer">
                    <div class="small">Logeado como:</div>
                    <div class="text-white">
                        <?php
                        if (isset($_SESSION['user'])) {
                            $nombre = strstr($_SESSION['user'], ' ', true);
                            echo $_SESSION['user'];
                        } else {
                            echo "Invitado";
                        }
                        ?>
                    </div>
                </div>

            </nav>
        </div>
        <div id="layoutSidenav_content">
            <!-----CONTENIDO AQUIIIIIIII -------------------------->
            <!---- AGREGAR PUBLICACION ---->
            <div class="container mt-5">
                <div class="col-md-12 col-sm-12 ">
                    <div class="row">
                        <div class="col-md-3 col-sm-12">
                            <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                                <div class="card-header text-center">Usuarios</div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-4 offset-2">
                                            <i class="fas fa-users fa-5x"></i>
                                        </div>
                                    </div>
                                    Usuarios Registrados :
                                    <b> <?php echo $fila['totalusers'] ?></b>
                                    <div class="row">
                                        <a href="usuarios.php" button class="btn btn-success ml-5 mt-2">Administrar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        <div class="col-md-3 col-sm-12">
                            <div class="card text-white bg-danger mb-3" style="max-width: 18rem;">
                                <div class="card-header text-center">Publicaciones reportadas</div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-4 offset-2">
                                        <i class="fas fa-thumbs-down fa-5x"></i>
                                        </div>
                                    </div>
                                    Reportes:
                                    <b><?php echo $fila3['totalreps'] ?></b>
                                    <div class="row">
                                        <a href="admin_report.php" class="btn btn-success ml-5 mt-2">Ver reportes</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
                                <div class="card-header text-center">Categorias</div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-4 offset-3">
                                        <i class="fas fa-poll fa-5x text-center"></i>
                                        </div>
                                    </div>
                                    Categorias:
                                        <b><?php echo $fila4['totalcat'] ?></b>
                                    <div class="row">
                                        <a href="categorias.php" class="btn btn-success ml-5 mt-2">Administrar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include("footer.php") ?>
        </div>
        <!-----/ FIIIIN  CONTENIDO ----------------------->

    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/chart-area-demo.js"></script>
    <script src="assets/demo/chart-bar-demo.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/datatables-demo.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="js/filtros.js"></script>
</body>

</html>