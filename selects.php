<?php include("bd/conexion.php"); 

$query = "SELECT id_region,nombre FROM region ORDER BY nombre ASC";
$regiones=  $conexion->query($query);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>test</title>
    <link href="css/styles.css" rel="stylesheet" />
</head>
<body>
 <?php include("navbar.php"); ?>
     
 <div class="container mt-5">
     <div class="row">
   <div class="col-md-6">
       <div class="form-group">
           <select name="regiones" id="regiones" class="form-control">
            <option value="0">Seleccionar Region</option>
            <?php while($row = $regiones->fetch_assoc()){ ?> 
             <option value="<?php echo $row['id_region'];?>">
             <?php echo $row['nombre'];?>
             </option>
            <?php } ?>
           </select>
       </div>
       <div class="form-group">
         <select name="ciudades" id="ciudades" class="form-control">
         <option value="0">Seleccionar Provincia</option>
         </select>
       </div>
       <div class="form-group">
           <select name="comunas" id="comunas" class="form-control">
           <option value="0">Seleccionar Comuna</option>
           </select>
       </div>
   </div>
   <div class="col-md-6">
       <table class="table table-striped table-bordered">
           <thead class="thead-light">
               <tr>
                   <th>Nombre</th>
                   <th>Ciudad</th>
               </tr>
           </thead>
           <tbody id="users">
               
           </tbody>
       </table>
   </div>
</div>
 </div>
</body>
</html>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
<script>

$(document).ready(function(){
     

    //ajax para actualizar select de region y provincia
    $("#regiones").change(function(){
        
        $('#comunas').find('option').remove().end().append(
            '<option value="nadaxd"></option>').val('nadaxd');
       


        $("#regiones option:selected").each(function(){
            id_region = $(this).val();
            $.post("bd/ajax/getCiudad.php",{id_region:id_region
            },function(data){
               
               $("#ciudades").html(data);
            });
        });
     
     
         
    });
       //ajax para actualizar select de provincia y comunas
    $("#ciudades").change(function(){
       $("#ciudades option:selected").each(function(){
           id_ciudad = $(this).val();
           $.post("bd/ajax/getComuna.php",{id_ciudad:id_ciudad
           },function(data){
               
              $("#comunas").html(data);   
           });
       });
   });

   //ajax para devolver usuario de una comuna

   $('#comunas').change(function(){
     $('#comunas option:selected').each(function(){
        id_comuna = $(this).val();
        $.post("bd/ajax/getUser.php",{id_comuna:id_comuna},
        function(data){
          console.log(data);
         $("#users").html(data);
        });
     });
   });



});
</script>





