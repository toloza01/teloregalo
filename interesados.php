<?php include('bd/conexion.php');


if(!isset($_SESSION['id_user'])){
    header("location: index.php");
}
$rut = $_SESSION['id_user'];
$post = $conexion->query("SELECT * FROM posts p,interesadosPub i WHERE p.rut_user='$rut' AND p.id_post=i.id_post");




?>


<link href="css/styles.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.0/css/bootstrap-slider.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.0/bootstrap-slider.min.js"></script>
<?php include("navbar.php"); ?>
<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
            <div class="sb-sidenav-menu">
                <div class="nav">
                    <?php if (isset($_SESSION['admin'])) { ?>
                        <div class="sb-sidenav-menu-heading text-white">Admin</div>
                        <a class="nav-link" href="admin.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                            Administracion
                        </a>
                    <?php  }  ?>


                    <div class="sb-sidenav-menu-heading text-white">Apoya a la comunidad</div>
                    <a class="nav-link active" href="newPost.php">
                        <div class="sb-nav-link-icon"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z" />
                                <path fill-rule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z" />
                                <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                            </svg></div>
                        Nueva publicacion
                    </a>
                    <a href="interesados.php" class="nav-link active">
                        <div class="sb-nav-link-icon">
                            <i class="fas fa-users"></i>
                        </div>
                        Mis interesados
                    </a>
                    
                </div>
            </div>

            <div class="sb-sidenav-footer">
                <div class="small">Logeado como:</div>
                <div class="text-white">
                    <?php
                    if (isset($_SESSION['id_user'])) {
                        $nombre = strstr($_SESSION['user'], ' ', true);
                        echo $_SESSION['user'];
                    } else {
                        echo "Invitado";
                    }
                    ?>
                </div>
            </div>
        </nav>
    </div>
    <div id="layoutSidenav_content">
        <div class="container">
            
          <div class="row mt-2">
              <div class="col-md-12">
                  <div class="card">
                     <div class="card-header">
                         <div class="text-center">
                             <h2>Mis Interesados</h2>
                         </div>
                     </div>
                     <div class="card-body">
                         <table class="table table-dark" id="tabla-interesados">
                             <thead class="thead-light">
                                 <tr>
                                     <th>ID</th>
                                     <th>Titulo Post</th>
                                     <th>Fecha Interes</th>
                                     <th>Email</th>
                                     <th>Telefono</th>
                                 </tr>
                             </thead>
                             <tbody>
                                 <?php while($postf= $post->fetch_assoc()){
                                     $user = $conexion->query("SELECT * FROM usuarios WHERE rut='$postf[id_user]'");
                                     while($userf = $user->fetch_assoc()){
                                    ?>
                                 <tr>
                                     <td><?php echo $postf['id'] ?></td>
                                     <td><?php echo $postf['titulo'] ?></td>
                                     <td><?php echo $postf['fecha_int'] ?></td>
                                     <td><?php echo $userf['email'] ?></td>
                                     <td><?php echo $userf['telefono'] ?></td>
                                 </tr>
                                 <?php }} ?>
                             </tbody>
                             
                         </table>
                     </div>
                  </div>
              </div>
          </div>
        </div>




    <?php include("footer.php"); ?>
     </div>            
</div>


<script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="js/scripts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
<script src="assets/demo/chart-area-demo.js"></script>
<script src="assets/demo/chart-bar-demo.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="assets/demo/datatables-demo.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="js/filtros2.js"></script>

<script>
    $(document).ready(function(){
        var tabla= $('#tabla-interesados').DataTable({
    "language": {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "buttons": {
            "copy": "Copiar",
            "colvis": "Visibilidad"
        }
    },
});





    });
</script>

