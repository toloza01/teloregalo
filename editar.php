<?php require('bd/conexion.php');
    $rut=$_GET['rut'];
    $consulta_rut = "select * FROM usuarios WHERE rut='$rut'";
    $query = mysqli_query($conexion, $consulta_rut);
    $row= mysqli_fetch_row($query);
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Editar perfil - Teloregalo</title>
        <link href="css/styles.css" rel="stylesheet" />
        <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
        
    </head>
    <body class="sb-nav-fixed">
    <?php include("navbar.php"); ?>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <?php if (isset($_SESSION['admin'])) { ?>
                            <div class="sb-sidenav-menu-heading text-white">Admin</div>
                            <a class="nav-link" href="admin.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Administracion
                            </a>
                        <?php  }  ?>                       
                        <div class="sb-sidenav-menu-heading text-white">Apoya a la comunidad</div>
                        <a class="nav-link active" href="newPost.php">
                            <div class="sb-nav-link-icon"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z" />
                                    <path fill-rule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z" />
                                    <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                </svg></div>
                            Nueva publicacion
                        </a>
                        <?php if(isset($_SESSION['id_user'])){ ?>  
                        <a href="interesados.php" class="nav-link active">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-users"></i>
                            </div>
                            Mis interesados
                        </a>
                    <?php } ?>
                    </div>
                </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logeado como:</div>
                       <div class="text-white">
                       <?php
                        if (isset($_SESSION['user'])) {
                            $nombre = strstr($_SESSION['user'], ' ', true);
                            echo $_SESSION['user'];
                        } else {
                            echo "Invitado";
                        }
                        ?>
                       </div>
                    </div>
                </nav>
            </div>
        <div id="layoutSidenav_content">
            <!-----CONTENIDO AQUIIIIIIII -------------------------->
          <div class="container">
                  <div class="col-md-12 mt-5">
                  <div class="card">
            <div class="card-header bg-primary text-white text-center">
                <h3>Editar usuario</h3>
            </div>
            <div class="card-body">
            
           <form action="userUpdate.php?rut=<?php echo $_GET['rut'];?> " method="POST">
             <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                    <label for="rut">Rut</label>
                    <input type="text" class="form-control" id="rut"  required oninput="checkRut(this)" name="rut" disabled value="<?php echo $row[0] ?>">
                </div>
                <div class="form-group">
                    <label for="nombre">Nombre y Apellido</label>
                    <input type="text" name="nombre" id="nombre" placeholder="Ingrese su nombre completo" class="form-control" value="<?php echo $row[1] ?>">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" class="form-control" placeholder="Ingrese su coreo electronico" value="<?php echo $row[2] ?>">
                </div>
                <div class="form-group">
                    <label for="direccion">Direccion</label>
                    <input type="text" name="direccion" id="direccion" class="form-control" placeholder="Ingrese su direccion" value="<?php echo $row[5] ?>">
                </div>
                <div class="form-group">
                    <label for="ciudad">Ciudad</label>
                    <select name="ciudad" id="ciudad" class="form-control">
                      <option value="Concepcion">Concepción</option>
                      <option value="Arauco">Arauco</option>
                      <option value="San Pedro de la paz">San pedro de la paz</option>
                    </select>
                </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="nac">Fecha de nacimiento</label>
                        <input type="date" class="form-control" id="nac" value="<?php echo $row[8];?>" name="nac" disabled >
                    </div>
                    <div class="form-group">
                        <label for="sexo">Sexo</label>
                        <select name="sexo" id="sexo" class="form-control" disabled>
                            <option value="M">Masculino</option>
                            <option value="F">Femenino</option>
                            <option value="O">Otro</option>
                        </select>
                    </div>
                    <div class="form-group">
                      <button type="submit" name="actualizar" id="actualizar" class="btn btn-block mt-5 btn-outline-primary">Actualizar</button>
                    </div>
                </div>
            </div>
            </form>
            </div>
          </div>  
          </div>
        </div>
               <!-----/ FIIIIN    CONTENIDO -------------------------->
               <?php  include("footer.php"); ?>
            </div>
        </div>
      
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/chart-area-demo.js"></script>
        <script src="assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/datatables-demo.js"></script>
        <script src="js/scripts.js"></script>
        <script src="js/validarRut.js"></script>
        <script src="js/filtros.js"></script>
      
    </body>
</html>
