$(document).ready(function() {
    //ajax para actualizar select de region y provincia
    $("#regiones").change(function() {
        $('#comunas').find('option').remove().end().append(
            '<option value="0"></option>').val('0');
        $("#regiones option:selected").each(function() {
            id_region = $(this).val();
            $.post("bd/ajax/getCiudad.php", {
                id_region: id_region
            }, function(data) {
                console.log(data);
                $("#ciudades").html(data);

            });
        });
    });
    //ajax para actualizar select de provincia y comunas
    $("#ciudades").change(function() {
        $("#ciudades option:selected").each(function() {
            id_ciudad = $(this).val();
            $.post("bd/ajax/getComuna.php", {
                id_ciudad: id_ciudad
            }, function(data) {
                console.log(data);
                $("#comunas").html(data);
            });
        });
    });
    //ajax para regiones publicador
    $("#regiones2").change(function() {
        $('#comunas2').find('option').remove().end().append(
            '<option value="0"></option>').val('0');
        $("#regiones2 option:selected").each(function() {
            id_region = $(this).val();
            $.post("bd/ajax/getCiudad.php", {
                id_region: id_region
            }, function(data) {
                console.log(data);
                $("#ciudades2").html(data);

            });
        });
    });
    //ajax para actualizar select de provincia y comunas
    $("#ciudades2").change(function() {
        $("#ciudades2 option:selected").each(function() {
            id_ciudad = $(this).val();
            $.post("bd/ajax/getComuna.php", {
                id_ciudad: id_ciudad
            }, function(data) {
                console.log(data);
                $("#comunas2").html(data);
            });
        });
    });
});
//Agregar nombre al archivo de imagen a subir
$('#inputGroupFile02').on('change', function() {
    //get the file name
    var fileName = $(this).val();
    //replace the "Choose a file" label
    $(this).next('.custom-file-label').html(fileName);
});
//Filtro de busqueda 
$('#btn-buscador').click(function(e) {
    e.preventDefault();
    var busqueda = $('#buscador').val();
    var filtrados = $('.filtrados');
    var cargando = $('.cargando');
    $.ajax({
        type: "POST",
        url: "bd/ajax/buscador.php",
        data: {
            busqueda: busqueda
        },
        beforeSend: function(){
          $(".lista").css("display", "none");
          $(".filtrados").html('');
          cargando.css("display","block");
        },
        success: function(data) {
            cargando.css("display","none");
            $(".filtrados").append(data);
        }
    });
 });
//Filtro de busqueda Navbar 
$('#btn-buscador2').click(function(e) {
    e.preventDefault();
    var busqueda = $('#buscador2').val();
    
   
    var filtrados = $('.filtrados');
    var cargando = $('.cargando');
    $.ajax({
        type: "POST",
        url: "bd/ajax/buscador.php",
        data: {
            busqueda: busqueda
        },
        beforeSend: function(){
          $(".lista").css("display", "none");
          $(".filtrados").html('');
          cargando.css("display","block");
        },
        success: function(data) {
            cargando.css("display","none");
            $(".filtrados").append(data);
        }
    });
 });
//filtros por tipo
$('#btn-filtrar').click(function(e) {
    e.preventDefault();
    var form = $('#form-filtros').serialize();
    var filtrados = $('.filtrados');
    var cargando = $('.cargando');
    $.ajax({
        type:"POST",
        url: "bd/ajax/filtros.php",
        data: form,
        beforeSend: function(){
            $(".lista").css("display", "none");
            $(".filtrados").html('');
            cargando.css("display","block");
        },
        success: function(data){
            cargando.css("display","none");
            $(".filtrados").append(data);
        }
    });
});