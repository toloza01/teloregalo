$(document).ready(function() {
   
var tabla= $('#tabla').DataTable({
    "language": {
        "sProcessing": "Procesando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "No se encontraron resultados",
        "sEmptyTable": "Ningún dato disponible en esta tabla",
        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        },
        "buttons": {
            "copy": "Copiar",
            "colvis": "Visibilidad"
        }
    },
});

});
function confirmar(id_categoria) {
    Swal.fire({
        title: 'Estas seguro?',
        text: "Esta accion no se puede deshacer!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borralo!'
      }).then((result) => {
        if (result.value) {
          eliminar(id_categoria);
          
        }
      });
}

function eliminar(id_categoria){
  
    $.ajax({
      type:"POST",
      url: "bd/ajax/eliminar_categoria.php",
      data:{'id_categoria':id_categoria} ,
      success: function(data){
        console.log(data);
        if (data == 1) {
            Swal.fire({
                icon: 'success',
                title: 'Eliminación exitosa',
                text: "Eliminado",
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Continuar'
            }).then((result) => {
                if (result.value) {
                   console.log("categoria eliminada!");
                   location.reload();
                }
            })

        } else {
            Swal.fire({
                icon: 'error',
                title: 'No se ha podido eliminar',
                text: 'Ocurrió un error interno'
            })
        }
      },
    
    });
   

}

$('#add-category').click(function(){ 
    $('#modal-title').html("Agregar Categoria");
    $('#category-form').trigger('reset');
    $('#modal-crud').modal('show');
});

$('#btn-category').click(function(e){
    e.preventDefault();
    var nombre = $('#nombre').val();
    var descripcion = $('#descripcion').val();
 
    $.ajax({
        type: "POST",
        data: {'nombre': nombre,'descripcion':descripcion},
        url: "bd/guardarCategoria.php",
        success: function(data){
            console.log(data);
            if (data == 1) {
                Swal.fire({
                    icon: 'success',
                    title: 'Nueva categoria',
                    text: "La categoria se ha agregado exitosamente!",
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Continuar'
                }).then((result) => {
                    if (result.value) {
                        location.reload();
                    }
                })
    
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'No se ha podido agregar la categoria',
                    text: 'Ocurrió un error interno'
                })
            }

        }
    });

});