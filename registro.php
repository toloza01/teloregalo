<?php require('bd/conexion.php');

if (isset($_SESSION['user'])) {
    header("location: index.php");
    die();
}

$query = "SELECT id_region,nombre FROM region ORDER BY nombre ASC";
$regiones =  $conexion->query($query);
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Registro - Teloregalo</title>
    <link href="css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
</head>

<body class="sb-nav-fixed">
    <?php include("navbar.php"); ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
            <div class="sb-sidenav-menu">
                    <div class="nav">
                        <?php if (isset($_SESSION['admin'])) { ?>
                            <div class="sb-sidenav-menu-heading text-white">Admin</div>
                            <a class="nav-link" href="admin.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Administracion
                            </a>
                        <?php  }  ?>                       
                        <div class="sb-sidenav-menu-heading text-white">Apoya a la comunidad</div>
                        <a class="nav-link active" href="newPost.php">
                            <div class="sb-nav-link-icon"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z" />
                                    <path fill-rule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z" />
                                    <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                </svg></div>
                            Nueva publicacion
                        </a>
                    </div>
                </div>
                <div class="sb-sidenav-footer">
                    <div class="small">Logeado como:</div>
                    <?php if (!isset($_SESSION['user'])) { ?>
                        <div class="text-white"> Invitado</div>
                    <?php } else { ?>
                        <div class="text-white"> Usuario</div>
                    <?php } ?>
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <!-----CONTENIDO AQUIIIIIIII -------------------------->
            <div class="container shadow-lg p-3 mb-5 mt-3" id="contenedor">
        <div class="col-md-12">
            <div class="row form">
                <div class="col-md-3">
                    <img height="100%" width="100%" style="margin-left: -6%;" src="https://cdn.computerhoy.com/sites/navi.axelspringer.es/public/styles/480/public/media/image/2020/05/fondos-pantalla-xiaomi-mi-note-10-1940571.jpg?itok=4-FY4cFb"     alt="">
                </div>
                <div class="col-md-9">
                  <div class="container">
                      <div class="row">
                          <div class="col-md-12">
                            <div class="col-md-6 offset-4">
                                <h3>Registro de usuarios</h3>                 
                            </div>
                            <div class="col-md-6 offset-3">
                                <hr width="65%">
                             </div>
                          </div>
                      </div>
                      <?php if(isset($_SESSION['error-register'])){ ?>
                        <div class="row">
                             <div class="col-md-12">
                                 <div class="alert alert-danger">
                                 <div class="text-center">
                                    <b><?=$_SESSION['error-register']?></b>
                                 </div>
                                 </div>
                                 <hr width="80%">
                             </div>
                        </div>

                      <?php session_unset(); } ?> 
                     
                      <div class="row">
                          <div class="container ">
                            <form action="bd/registrar.php" method="POST">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="rut">Rut</label>
                                            <input type="text" class="form-control" id="rut" required oninput="checkRut(this)" name="rut" placeholder="Ingrese su RUT, ej: 11111111-1" minlength="9" maxlength="10">
                                        </div>
                                        <div class="form-group">
                                            <label for="nombre">Nombre</label>
                                            <input type="text" name="nombre" id="nombre" required placeholder="Ingrese su nombre completo" class="form-control" minlength="10" maxlength="30">
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" name="email" id="email" required class="form-control" placeholder="Ingrese su coreo electronico">
                                        </div>
                                        <div class="form-group">
                                            <label for="direccion">Direccion</label>
                                            <input type="text" name="direccion" id="direccion" required class="form-control" placeholder="Ingrese su direccion">
                                        </div>
                                        <div class="form-group">
                                            <label for="regiones">Region</label>
                                            <select name="regiones" id="regiones" class="form-control">
                                                <option value="0">Seleccionar Region</option>
                                                <?php while ($row = $regiones->fetch_assoc()) { ?>
                                                    <option value="<?php echo $row['id_region']; ?>">
                                                        <?php echo $row['nombre']; ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="ciudades">Provincia</label>
                                            <select name="ciudades" id="ciudades" class="form-control">
                                                <option value="0">Seleccionar Provincia</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="comunas">Comuna</label>
                                            <select name="comunas" id="comunas" class="form-control">
                                                <option value="0">Seleccionar Comuna</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nac">Fecha de nacimiento</label>
                                            <input type="date" class="form-control" min="1930-01-01" max="2003-01-01" value="2000-01-01" required id="nac" name="nac">
                                        </div>
                                        <div class="form-group">
                                            <label for="pass">Contraseña</label>
                                            <input type="password" class="form-control" required id="pass"  name="pass" minlength="3">
                                        </div>
                                        <div class="form-group">
                                            <label for="passc">Confirmar Contraseña</label>
                                            <input type="password" class="form-control" required id="passc" name="passc" minlength="3">
                                        </div>
                                        <div class="form-group">
                                            <label for="sexo">Sexo</label>
                                            <select name="sexo" id="sexo" class="form-control">
                                                <option value="M">Masculino</option>
                                                <option value="F">Femenino</option>
                                                <option value="O">Otro</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="numero">Celular</label>
                                            <input type="number" class="form-control" name="numero" id="numero" placeholder="Ingrese su numero de 9 digitos ej: 912345678" minlength="9" maxlength="12">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8 offset-2">
                                        <button type="submit" class="btn btn-block btn-outline-primary btn-lg" name="registrar" id="registrar">Registrar</button>
                                    </div>
                                </div>
                            </form>
                          </div>
                      </div>
                  </div>
                </div>
             </div>
        </div>
    </div>

           
            <!-----/ FIIIIN    CONTENIDO -------------------------->
            <?php include("footer.php"); ?>
        </div>
    </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/chart-area-demo.js"></script>
    <script src="assets/demo/chart-bar-demo.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/datatables-demo.js"></script>
    <script src="js/validarRut.js"></script>
    <script src="js/filtros.js"></script>
   
</body>

</html>