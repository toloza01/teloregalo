<?php
include('bd/conexion.php');

if (!isset($_SESSION['admin'])) {
    header("location: index.php");
}

$categorias = $conexion->query("SELECT * FROM categorias");


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Categorias - Teloregalo</title>
    <link href="css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
</head>

<body class="sb-nav-fixed">
    <?php include("navbar.php") ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
            <div class="sb-sidenav-menu">
                    <div class="nav">
                        <?php if (isset($_SESSION['admin'])) { ?>
                            <div class="sb-sidenav-menu-heading text-white">Admin</div>
                            <a class="nav-link" href="admin.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Administracion
                            </a>
                        <?php  }  ?>
                        <div class="sb-sidenav-menu-heading text-white">Apoya a la comunidad</div>
                        <a class="nav-link active" href="newPost.php">
                            <div class="sb-nav-link-icon"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z" />
                                    <path fill-rule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z" />
                                    <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                </svg></div>
                            Nueva publicacion
                        </a>
                        <?php if(isset($_SESSION['id_user'])){ ?>  
                        <a href="interesados.php" class="nav-link active">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-users"></i>
                            </div>
                            Mis interesados
                        </a>
                    <?php } ?>
                    </div>
                </div>
                <div class="sb-sidenav-footer">
                    <div class="small">Logeado como:</div>
                    <div class="text-white">
                        <?php
                        if (isset($_SESSION['user'])) {
                            $nombre = strstr($_SESSION['user'], ' ', true);
                            echo $_SESSION['user'];
                        } else {
                            echo "Invitado";
                        }
                        ?>
                    </div>
                </div>
            
            
            
            
            </div>
        <div id="layoutSidenav_content">
            <!-----CONTENIDO AQUIIIIIIII -------------------------->
            <div class="container mt-5">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-dark" role="alert">
                                <h3 class="text-center">Administrar Categorias</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php if(isset($_SESSION['editar-success'])){?>
                                  <div class="alert alert-success text-center" role="alert">
                                      <b><?php echo $_SESSION['editar-success']; unset($_SESSION['editar-success']);?></b> 
                                  </div>
                                <?php } ?>  
                            <table id="tabla" class="table table-bordered">
                                <thead>
                                    <th>Nombre</th>
                                    <th>Descripcion</th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody>
                                    <?php if ($categorias->num_rows > 0) {
                                        while ($categoria = $categorias->fetch_assoc()) {
                                    ?>
                                            <tr>
                                                <td><?= $categoria['nombre'] ?></td>
                                                <td><?= $categoria['descripcion'] ?></td>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <a class="btn btn-danger btn-block text-white" onclick="confirmar(<?php echo $categoria['id_categoria'] ?>)" id="eliminar"><i class="fas fa-trash-alt"></i> Eliminar</a>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <a class="btn btn-primary btn-block text-white" href="editarCategoria.php?id_categoria=<?=$categoria['id_categoria']?>" id="editar"><i class="fas fa-pencil-alt"></i> Editar</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                    <?php }
                                    }  ?>

                                </tbody>
                            </table>
                            <div id="modal-crud" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="modal-title">Title</h5>
                                            <button class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="nombre">Nombre Categoria:</label>
                                                <input type="text" name="nombre" id="nombre" placeholder="Computadores,Ocio,Servicios..." class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="descripcion">Descripcion Categoria: </label>
                                                <textarea name="descripcion" id="descripcion" class="form-control" style="max-height: 200px; min-height:100px;" cols="30" rows="5"></textarea>
                                            </div>
                                            <div class="form group">
                                                <button id="btn-category" class="btn btn-success btn-block">Guardar Categoria</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div id="modal-crud-editar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="modal-title">Editar Categoria</h5>
                                            <button class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="nombre">Nombre Categoria:</label>
                                                <input type="text" name="nombre" id="nombre" placeholder="Computadores,Ocio,Servicios..." class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="descripcion">Descripcion Categoria: </label>
                                                <textarea name="descripcion" id="descripcion" class="form-control" style="max-height: 200px; min-height:100px;" cols="30" rows="5"></textarea>
                                            </div>
                                            <div class="form group">
                                                <button id="btn-category" class="btn btn-success btn-block">Editar Categoria</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2 mb-5">
                        <div class="col-md-12">
                            <button class="btn btn-block btn-dark" id="add-category" style="background: black; color:white;">Agregar Categoria</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php include("footer.php") ?>
        </div>
        <!-----/ FIIIIN  CONTENIDO ----------------------->
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/chart-area-demo.js"></script>
    <script src="assets/demo/chart-bar-demo.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/datatables-demo.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="js/filtros.js"></script>
    <script src="js/categorias.js"></script>


</body>

</html>