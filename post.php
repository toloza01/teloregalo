<?php
include('bd/conexion.php');

$post = "SELECT * FROM posts WHERE id_post='$_GET[id]'";
$query = mysqli_query($conexion, $post);
$row = mysqli_fetch_row($query);
$nombreq = "SELECT * FROM usuarios WHERE rut='$row[5]'";
$user = mysqli_query($conexion, $nombreq);
$usuario = mysqli_fetch_row($user);
$comunaQ = "SELECT nombre FROM comuna WHERE id_comuna='$row[8]'";
$comunaM = mysqli_query($conexion, $comunaQ);
$comuna = $comunaM->fetch_assoc();


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Publicación - Teloregalo</title>
    <link href="css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
</head>

<body class="sb-nav-fixed">
    <?php include("navbar.php"); ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <?php if (isset($_SESSION['admin'])) { ?>
                            <div class="sb-sidenav-menu-heading text-white">Admin</div>
                            <a class="nav-link" href="admin.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Administracion
                            </a>
                        <?php  }  ?>
                        <div class="sb-sidenav-menu-heading text-white">Apoya a la comunidad</div>
                        <a class="nav-link active" href="newPost.php">
                            <div class="sb-nav-link-icon"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z" />
                                    <path fill-rule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z" />
                                    <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                </svg></div>
                            Nueva publicacion
                        </a>
                        <?php if(isset($_SESSION['id_user'])){ ?>  
                        <a href="interesados.php" class="nav-link active">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-users"></i>
                            </div>
                            Mis interesados
                        </a>
                    <?php } ?>
                    </div>
                </div>

                <div class="sb-sidenav-footer">
                    <div class="small">Logeado como:</div>
                    <div class="text-white">
                        <?php
                        if (isset($_SESSION['user'])) {
                            $nombre = strstr($_SESSION['user'], ' ', true);
                            echo $_SESSION['user'];
                        } else {
                            echo "Invitado";
                        }
                        ?>
                    </div>
                </div>

            </nav>
        </div>
        <div id="layoutSidenav_content">
            <!-----CONTENIDO AQUIIIIIIII -------------------------->
            <div class="container-fluid mt-5">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header text-center">
                                        <h4><?php echo $row[1] ?></h4>
                                    </div>
                                    <div class="card-body">
                                        <p><?php echo $row[2] ?></p>
                                        <br>
                                        <?php if (isset($row[3]) && $row[3] != null) { ?>
                                            <div class="card mt-2">
                                                <img style="max-height: 400px; width:100%;" src="img/uploads/<?= $row[3] ?>" alt="">
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header text-center">
                                        <h4>Contacto</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <b>Nombre: </b> <?php echo $usuario[1] ?>
                                        </div>
                                        <div class="form-group">
                                            <b>Email: </b> <?php echo $usuario[2] ?>
                                        </div>

                                        <div class="form-group">
                                            <b>Comuna: </b> <?php echo $comuna['nombre'] ?>
                                        </div>
                                        <div class="form-group">
                                            <b>Celular: </b> <?php echo $usuario[4] ?>
                                        </div>
                                    </div>
                                </div>
                                <?php if (isset($_SESSION['id_user']) && $usuario[0] != $_SESSION['id_user']) { ?>
                                    <div class="user" style="display: none;">
                                        <?= $_SESSION['id_user'] ?>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form id="form-interes" method="post">
                                                <input type="hidden" id="post" name="post" value="<?= $_GET['id'] ?>">
                                                <input type="hidden" id="interesado" value="<?= $_SESSION['id_user'] ?>">
                                                <a class="btn btn-success btn-block btn-lg btn-md mt-2 text-white" type="submit" id="btn-interes">
                                                    <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                                    Me interesa</a>
                                            </form>

                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <?php if ((isset($_SESSION['id_user']) && $usuario[0] == $_SESSION['id_user']) || isset($_SESSION['admin'])) {  ?>
                                            <a title="Eliminar Post" class="btn btn-danger btn-block" href="bd/eliminarPub.php?id_post=<?php echo $row[0] ?>" name="eliminarPub" id="eliminarPub"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z" />
                                                </svg>Eliminar Publicacion</a>
                                        <?php } ?>
                                    </div>
                                  
                                    <?php if ((isset($_SESSION['id_user']) && $usuario[0] == $_SESSION['id_user']) ) {  ?>
                                        <div class="col-md-6">
                                        <a href="bd/editarPub.php?id_post=<?php echo $row[0]?>" name="editarPub" class="btn btn-primary btn-block">Editar Publicacion</a>
                                        
                                        </div>
                                    <?php } ?>
                                    <div class="col-md-6">
                                        <?php if ((isset($_SESSION['id_user']) && $usuario[0] != $_SESSION['id_user'])) {  ?>
                                            <a title="Reportar publicacion" class="btn btn-danger btn-block" href="bd/reportarPost.php?id_post=<?php echo $row[0] ?>" name="reportarPost" id="reportarPost">
                                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-flag-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M3.5 1a.5.5 0 0 1 .5.5v13a.5.5 0 0 1-1 0v-13a.5.5 0 0 1 .5-.5z" />
                                                    <path fill-rule="evenodd" d="M3.762 2.558C4.735 1.909 5.348 1.5 6.5 1.5c.653 0 1.139.325 1.495.562l.032.022c.391.26.646.416.973.416.168 0 .356-.042.587-.126a8.89 8.89 0 0 0 .593-.25c.058-.027.117-.053.18-.08.57-.255 1.278-.544 2.14-.544a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-.5.5c-.638 0-1.18.21-1.734.457l-.159.07c-.22.1-.453.205-.678.287A2.719 2.719 0 0 1 9 9.5c-.653 0-1.139-.325-1.495-.562l-.032-.022c-.391-.26-.646-.416-.973-.416-.833 0-1.218.246-2.223.916A.5.5 0 0 1 3.5 9V3a.5.5 0 0 1 .223-.416l.04-.026z" />
                                                </svg>
                                                Reportar Publicacion
                                            </a>
                                        <?php } ?>
                                    </div>


                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-12 ">
                        <div id="disqus_thread"></div>
                        <script>
                            /**
                             *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                             *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                            /*
                            var disqus_config = function () {
                            this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                            this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                            };
                            */
                            (function() { // DON'T EDIT BELOW THIS LINE
                                var d = document,
                                    s = d.createElement('script');
                                s.src = 'https://teloregalo.disqus.com/embed.js';
                                s.setAttribute('data-timestamp', +new Date());
                                (d.head || d.body).appendChild(s);
                            })();
                        </script>
                        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                    </div>
                </div>
            </div>
            <!-----/ FIIIIN    CONTENIDO -------------------------->
            <?php include("footer.php"); ?>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/chart-area-demo.js"></script>
        <script src="assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/datatables-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>


</body>

</html>
<script>
    $(document).ready(function() {

        $('#btn-interes').click(function() {
            var post = $('#post').val();
            var interesado = $('#interesado').val();
            $.ajax({
                url: "bd/meinteresa.php",
                method: "post",
                data: {
                    'post': post,
                    'interesado': interesado
                },
                success: function(data) {
                    if (data=="success") {
                        Swal.fire({
                            icon: 'success',
                            title: 'Agregado!',
                            text: "Se te ha agregado a la lista de interesados. El dueño de la publicacion se comunicara contigo en caso de ser aceptado.",
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Continuar'
                        });
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Ha ocurrido un error',
                            text: 'No se pudo agregar. Verifica que no hayas postulado antes a este post.'
                        })
                    }
                }
            });
        });









        /* Swal.fire({
                title: "Agregado!",
                text: "El posteador se comunicara contigo en caso de ser aceptado!",
                icon: "success",
                confirmButtonText: 'OK!'
            }); */
    });
</script>

} ?>