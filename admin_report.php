<?php
include('bd/conexion.php');

if (!isset($_SESSION['admin'])) {
    header("location: index.php");
}
$situacion = "SELECT * FROM reportes";
$consulta_reportes = $conexion->query($situacion);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Reportes - Teloregalo</title>
    <link href="css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
</head>

<body class="sb-nav-fixed">
    <?php include("navbar.php") ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
            <div class="sb-sidenav-menu">
                    <div class="nav">
                        <?php if (isset($_SESSION['admin'])) { ?>
                            <div class="sb-sidenav-menu-heading text-white">Admin</div>
                            <a class="nav-link" href="admin.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Administracion
                            </a>
                        <?php  }  ?>
                        <div class="sb-sidenav-menu-heading text-white">Apoya a la comunidad</div>
                        <a class="nav-link active" href="newPost.php">
                            <div class="sb-nav-link-icon"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z" />
                                    <path fill-rule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z" />
                                    <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                </svg></div>
                            Nueva publicacion
                        </a>
                        <?php if(isset($_SESSION['id_user'])){ ?>  
                        <a href="interesados.php" class="nav-link active">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-users"></i>
                            </div>
                            Mis interesados
                        </a>
                    <?php } ?>
                    </div>
                </div>

                <div class="sb-sidenav-footer">
                    <div class="small">Logeado como:</div>
                    <div class="text-white">
                        <?php
                        if (isset($_SESSION['user'])) {
                            $nombre = strstr($_SESSION['user'], ' ', true);
                            echo $_SESSION['user'];
                        } else {
                            echo "Invitado";
                        }
                        ?>
                    </div>
                </div>

            </nav>
        </div>
        <div id="layoutSidenav_content">
            <!-----CONTENIDO AQUIIIIIIII -------------------------->

            <div class='table-responsive' style='padding: 4%; margin-bottom:100px;'>
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-primary alert-link">
                            <h3 class="text-center text-dark">Lista de publicaciones reportadas</h3>
                        </div>
                    </div>
                </div>
                <?php if (isset($_SESSION['msg'])) { ?>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <?php echo $_SESSION['msg'];
                        unset($_SESSION['msg']);
                        ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>
                <?php if (isset($_SESSION['mensaje'])) { ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <?php echo $_SESSION['mensaje'];
                        unset($_SESSION['mensaje']);
                        ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>
                <table id="tabla" class="table table-striped table-bordered">
                    <thead class='thead-dark'>
                        <tr>
                            <th width="5%">ID</th>
                            <th width="5%">ID post</th>
                            <th width="10%">Fecha reporte</th>
                            <th width="20%">Comentario</th>
                            <th width="10%">Motivo</th>
                            <th width="10%">Rut reportado</th>
                            <th width="20%">Nombre reportado</th>
                            <th width="5%">Reportes</th>
                            <th width="15%">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if ($consulta_reportes->num_rows > 0) {
                            while ($usuarios = $consulta_reportes->fetch_assoc()) {
                               $nombre=$conexion->query("SELECT * FROM usuarios WHERE rut='$usuarios[reportado]'");
                               $nombre_rep=$nombre->fetch_assoc();
                               $num_rep=$conexion->query("SELECT COUNT(*) as numer FROM reportes WHERE id_post='$usuarios[id_post]'");
                               $num=$num_rep->fetch_assoc();
                        ?>
                                <tr>
                                    <td><?php echo $usuarios['id_reporte'] ?></td>
                                    <td><?php echo $usuarios['id_post'] ?></td>
                                    <td><?php echo $usuarios['fecha_reporte'] ?></td>
                                    <td><?php echo $usuarios['comentario'] ?></td>
                                    <td><?php echo $usuarios['motivo'] ?></td>
                                    <td><?php echo $usuarios['reportado'] ?></td>
                                    <td><a href="perfil.php?id_user=<?= $usuarios['reportado'] ?>" title="Ver perfil usuario reportado"><?php echo $nombre_rep['nombre'] ?></a></td>
                                    <?php 
                                    if($num['numer']<3){ ?>
                                        <td><h6><b><?php echo $num['numer'] ?></b></h6> </td> <?php 
                                    }else{ ?>
                                    
                                    <td> <span class="badge badge-warning "><h6><b><?php echo $num['numer'] ?></b></h6></span> </td>
                                    <?php } ?>
                                    <td>
                                        <div class="row">
                                            <a href="post.php?id=<?php echo $usuarios['id_post']?>" class="btn btn-success btn-sm ml-3" title="Ver publicación"><i class="fas fa-eye"></i></a>
                                            <a href='#' onclick="preguntar(<?php echo $usuarios['id_post']?>)" class="btn btn-danger btn-sm ml-3" title="Eliminar publicación"><i class="fa fa-trash"></i></a>
                                        </div>
                                    </td>
                                    <script type="text/javascript">
                                        function preguntar(id)
                                        {
                                            if(confirm('¿Estás seguro que deseas borrar esta publicación?'))
                                            { window.location.href = "bd/eliminarPub2.php?id_post="+id; }
                                        }
                                    </script>
                                </tr>
                        <?php }
                        } ?>
                    </tbody>
                </table>
            </div>
            <!-----/ FIIIIN    CONTENIDO -------------------------->
            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class="text-muted">Copyright &copy; te lo regalo 2020</div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/datatables-demo.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="js/filtros.js"></script>
    <script>
        $(document).ready(function() {
            $('#tabla').DataTable({
             "language":
                {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "No hay publicaciones reportadas",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    },
                    "buttons": {
                        "copy": "Copiar",
                        "colvis": "Visibilidad"
                    }
                }
            });
        });
    </script>
</body>

</html>