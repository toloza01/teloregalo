<nav class="sb-topnav navbar navbar-expand navbar-dark bg-primary">
        <a class="navbar-brand" href="../index.php"><svg width="1em" class="mb-2" style="font-size:34px" height="1em" viewBox="0 0 16 16" class="bi bi-gift-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M3 2.5a2.5 2.5 0 0 1 5 0 2.5 2.5 0 0 1 5 0v.006c0 .07 0 .27-.038.494H15a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1h2.038A2.968 2.968 0 0 1 3 2.506V2.5zm1.068.5H7v-.5a1.5 1.5 0 1 0-3 0c0 .085.002.274.045.43a.522.522 0 0 0 .023.07zM9 3h2.932a.56.56 0 0 0 .023-.07c.043-.156.045-.345.045-.43a1.5 1.5 0 0 0-3 0V3z"/>
  <path d="M15 7v7.5a1.5 1.5 0 0 1-1.5 1.5H9V7h6zM2.5 16A1.5 1.5 0 0 1 1 14.5V7h6v9H2.5z"/>
</svg> <em>Te lo regalo</em> </a>
        <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>

        <ul class="navbar-nav ml-auto mr-md-0">
            <?php if (!isset($_SESSION['user'])) { ?>
                <li class="nav-item">
                    <a href="../login.php" class="nav-link active">Iniciar Sesion</a>
                </li>
                <li class="nav-item">
                    <a href="../registro.php" class="nav-link active">Registrarse</a>
                </li>

            <?php } else { ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="../perfil.php?id_user=<?php echo $_SESSION['id_user'] ?>">Perfil</a>
                        <a class="dropdown-item" href="../bd/logout.php">Cerrar Sesion</a>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </nav>
   
     