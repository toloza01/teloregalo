<?php
    include('bd/conexion.php');
    if (isset($_SESSION['user'])) {
        $rut = $_SESSION['id_user'];
        $admin = "SELECT * FROM rol_user WHERE rut_user='$rut' AND id_rol=1";
        $adminQ = mysqli_query($conexion, $admin);
        $adminr = mysqli_fetch_row($adminQ);
    }else{
        header("location: index.php");
    }
    //obtener usuario a enviar mensaje 
    $destinoMensaje = $_GET['user'];
    $postMensaje = $_GET['post'];
    $datosDestino = mysqli_query($conexion, "SELECT * FROM usuarios WHERE email='$destinoMensaje'");
    $datosPost = mysqli_query($conexion, "SELECT * FROM posts WHERE id_post='$postMensaje'");
    $destinoU = mysqli_fetch_row($datosDestino);
    $destinoP = mysqli_fetch_row($datosPost);


    $sql_usuarios = "SELECT * FROM users";
    $consulta_usuarios = $conexion->query($sql_usuarios);
    $sql_posts = "SELECT * FROM posts ORDER BY fecha DESC";
    $posts = $conexion->query($sql_posts);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Home</title>
    <link href="css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.0/css/bootstrap-slider.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.0/bootstrap-slider.min.js"></script>
</head>

<body class="sb-nav-fixed">
<?php include("navbar.php"); ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
            <div class="sb-sidenav-menu">
                    <div class="nav">
                        <?php if (isset($_SESSION['admin'])) { ?>
                            <div class="sb-sidenav-menu-heading text-white">Admin</div>
                            <a class="nav-link" href="admin.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Administracion
                            </a>
                        <?php  }  ?> 
                        <div class="sb-sidenav-menu-heading text-white">Apoya a la comunidad</div>
                        <a class="nav-link active" href="newPost.php">
                            <div class="sb-nav-link-icon"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z" />
                                    <path fill-rule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z" />
                                    <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                </svg></div>
                            Nueva publicacion
                        </a>
                        <?php if(isset($_SESSION['id_user'])){ ?>  
                        <a href="interesados.php" class="nav-link active">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-users"></i>
                            </div>
                            Mis interesados
                        </a>
                    <?php } ?>
                    </div>
                </div>
                <div class="sb-sidenav-footer">
                    <div class="small">Logeado como:</div>
                    <div class="text-white">
                        <?php
                        if (isset($_SESSION['user'])) {
                            $nombre = strstr($_SESSION['user'], ' ', true);
                            echo $_SESSION['user'];
                        } else {
                            echo "Invitado";
                        }
                        ?>
                    </div>
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <!-----CONTENIDO AQUIIIIIIII -------------------------->
            <!---- AGREGAR PUBLICACION -->
            <div class="container">
                <div class="row">
                    <div class="col-md-12 mt-2">
                        <div class="col-md-8 offset-2">
                            <div class="card">
                                <div class="card-header text-center">
                                    <h4>Contactar</h4>
                                </div>
                                <div class="card-body">
                                    <form action="bd/enviarMensaje.php" method="post">
                                        <div class="form-group">
                                            <label for="destino">Nombre destino:</label>
                                            <input type="text" class="form-control" name="nombre" value="<?php echo $destinoU[1] ?>" readonly="readonly" >
                                        </div>
                                        <div class="form-group">
                                            <label for="destino">Titulo Post:</label>
                                            <input type="text" class="form-control" name="titulo" value="<?php echo $destinoP[1] ?>" readonly="readonly">
                                        </div>
                                        <div class="form-group">
                                            <label for="remitente">Email Remitente</label>
                                            <input type="text" class="form-control" name="remitente" value="<?php echo $_SESSION['email_user'] ?>" >
                                        </div>
                                        <div class="form-group">
                                            <label for="destino">Email destinatario:</label>
                                            <input type="text" class="form-control" name="destino" value="<?php echo $destinoU[2] ?>" readonly="readonly">
                                        </div>
                                        <div class="form-group">
                                            <label for="mensaje">Mensaje: </label>
                                            <textarea name="mensaje" class="form-control" id="mensaje" cols="30" rows="8"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" name="enviar" class="btn btn-block btn-success btn-md">Enviar Mensaje</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-----/ FIIIIN    CONTENIDO -------------------------->
            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class="text-muted">Copyright &copy; te lo regalo 2020</div>

                    </div>
                </div>
            </footer>

        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/chart-area-demo.js"></script>
        <script src="assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/datatables-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script src="js/filtros.js"></script>

</body>

</html>