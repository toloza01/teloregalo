<?php
header('Content-Type: text/html; charset=UTF-8');

include('bd/conexion.php');

if (isset($_SESSION['user'])) {
    $rut = $_SESSION['id_user'];
    $admin = "SELECT * FROM rol_user WHERE rut_user='$rut' AND id_rol=1";
    $adminQ = mysqli_query($conexion, $admin);
    $adminr = mysqli_fetch_row($adminQ);
}
$sql_usuarios = "SELECT * FROM users";
$consulta_usuarios = $conexion->query($sql_usuarios);
$sql_posts = "SELECT * FROM posts ORDER BY fecha DESC LIMIT 12";
$posts = $conexion->query($sql_posts);
$query = "SELECT id_region,nombre FROM region ORDER BY nombre ASC";
$regiones =  $conexion->query($query);
$regiones2 =  $conexion->query($query);
$categorias = $conexion->query("SELECT * FROM categorias");
$categorias2 = $conexion->query("SELECT * FROM categorias");

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Home - Teloregalo</title>
    <link href="css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.0/css/bootstrap-slider.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.0/bootstrap-slider.min.js"></script>
</head>

<body class="sb-nav-fixed">
    <?php include("navbar.php"); ?>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <?php if (isset($_SESSION['admin'])) { ?>
                            <div class="sb-sidenav-menu-heading text-white">Admin</div>
                            <a class="nav-link" href="admin.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Administracion
                            </a>
                        <?php  }  ?>
                        <div class="sb-sidenav-menu-heading text-white">Apoya a la comunidad</div>
                        <a class="nav-link active" href="newPost.php">
                            <div class="sb-nav-link-icon"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z" />
                                    <path fill-rule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z" />
                                    <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                </svg></div>
                            Nueva publicacion
                        </a>
                    <?php if(isset($_SESSION['id_user'])){ ?>  
                        <a href="interesados.php" class="nav-link active">
                            <div class="sb-nav-link-icon">
                                <i class="fas fa-users"></i>
                            </div>
                            Mis interesados
                        </a>
                    <?php } ?>
                    </div>
                </div>

                <div class="sb-sidenav-footer">
                    <div class="small">Logeado como:</div>
                    <div class="text-white">
                        <?php
                        if (isset($_SESSION['id_user'])) {
                            $nombre = strstr($_SESSION['user'], ' ', true);
                            echo $_SESSION['user'];
                        } else {
                            echo "Invitado";
                        }
                        ?>
                    </div>
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <!-----CONTENIDO AQUIIIIIIII -------------------------->
            <div class="container bg-light">
                <div class="row">
                    <div class="col-md-8 offset-md-2 col-sm-12 mt-3">
                        <div class="input-group">
                            <input class="form-control p-2 py-2 border-right-0 border" id="busqueda" name="busqueda" placeholder="Busca lo que quieras!..." type="search">
                            <span class="input-group-append">
                                <button class="btn btn-dark border-left-0 border" id="btn-buscador" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-12 mt-3">
                        <button type="button" id="limpiar" class="btn btn-danger col-10 btn-md">Limpiar Filtros</button>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="container-fluid bg-light">
                        <form id="form-filtros" method="post">
                            <div class="row align-items-center justify-content-center">
                                <div class="col-md-2 pt-3">
                                    <div class="form-group">
                                        <select name="regiones" id="regiones" required class="form-control">
                                            <option value="0">Seleccionar Region</option>
                                            <?php while ($row2 = $regiones2->fetch_assoc()) { ?>
                                                <option value="<?php echo $row2['id_region']; ?>">
                                                    <?php echo $row2['nombre']; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 pt-3">
                                    <div class="form-group">
                                        <select name="ciudades" id="ciudades" required class="form-control">
                                            <option value="0">Seleccionar Provincia</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 pt-3">
                                    <div class="form-group">
                                        <select name="comunas" id="comunas" required class="form-control">
                                            <option value="0">Seleccionar Comuna</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 pt-3">
                                    <div class="form-group">
                                        <select class="form-control" required name="categoria" id="categoria">
                                            <option style="background-color:white;" value="0">Escoge una categoria</option>

                                            <?php while ($categoriaP = $categorias->fetch_assoc()) { ?>
                                                <option style="background-color:#dcdcc3; font-weight: bold;" value="<?php echo $categoriaP['id_categoria']; ?>">
                                                    <?php echo $categoriaP['nombre']; ?></b>
                                                </option>

                                            <?php } ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 pt-3">
                                    <div class="form-group">
                                        <select class="form-control" name="tipo" id="tipo">
                                            <option value="todos">Todos los anuncios</option>
                                            <option value="regalo">Regalo</option>
                                            <option value="busco">Busco</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-primary btn-block" id="btn-filtrar">Filtrar</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-12">
                    <?php if (isset($_SESSION['msg-delete'])) { ?>
                        <div class="alert alert-danger" role="alert">
                            <?php echo $_SESSION['msg-delete'];
                            unset($_SESSION['msg-delete']); ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-12">
                    <?php if (isset($_SESSION['success'])) { ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo $_SESSION['success'];
                            unset($_SESSION['success']); ?>
                        </div>

                    <?php } ?>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-12">
                    <?php if (isset($_SESSION['error'])) { ?>
                        <div class="alert alert-danger" role="alert">
                            <?php echo $_SESSION['error'];
                            unset($_SESSION['error']); ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="row mt-4">
                <div class="container-fluid bg-light">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="lista">
                                <div class="row">
                                    <?php
                                    if ($posts->num_rows > 0) {

                                        while ($row = $posts->fetch_assoc()) {
                                            $nombre = $conexion->query("SELECT * FROM usuarios WHERE rut='$row[rut_user]'");
                                            $nombreF = $nombre->fetch_assoc();
                                            $comuna = $conexion->query("SELECT * FROM comuna WHERE id_comuna='$row[id_comuna]'");
                                            $comunaF = $comuna->fetch_assoc();
                                            $categoria = mysqli_query($conexion, "SELECT * FROM categorizar WHERE id_post='$row[id_post]'");
                                            $estadoq = $conexion->query("SELECT * FROM estado_producto WHERE id_estado='$row[id_estado]'");
                                            $estado = $estadoq->fetch_assoc();
                                    ?>
                                            <!-----POST------>
                                            <div class="col-md-4 col-sm-6 mt-4">
                                                <div class="card shadow-lg" style="height: 500px; ">
                                                    <div class="card-header bg-light text-dark text-center ">
                                                        <b> <?= $row['titulo'] ?> </b>
                                                    </div>
                                                    <?php if (isset($row['imagen']) && $row['imagen'] != null) { ?>
                                                        <a href="post.php?id=<?= $row['id_post'] ?>"><img src="img/uploads/<?= $row['imagen'] ?>" width="100%" title="<?= $row['contenido'] ?>" height="240px" alt="post"></a>
                                                    <?php } else { ?>
                                                        <a href="post.php?id=<?= $row['id_post'] ?>"><img src="https://tripisia.id/assets/images/NoImage.png" width="100%" title="<?= $row['contenido'] ?>" height="240px" alt="img post"></a>
                                                    <?php } ?>
                                                    <div class="card-footer" style="height: 100%;">
                                                        <div class="row">
                                                            <i class="fas fa-street-view ml-2" style="font-size: 30px;"></i>
                                                            <h4 class="ml-2"><?= $comunaF['nombre'] ?></h4>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <span class="badge badge-primary ml-1"><?= $row['tipo_anuncio'] ?></span>
                                                                <span class="badge badge-success ml-2"><?=$estado['nombre']?></span>
                                                            </div>
                                                        </div>
                                                        <div class="row mt-1">
                                                            <div class="col-md-12">
                                                                <?php while ($categorias = $categoria->fetch_assoc()) {
                                                                    $nomCategoria = $conexion->query("SELECT nombre FROM categorias WHERE id_categoria='$categorias[id_categoria]'");
                                                                    $nomCategoriaF = $nomCategoria->fetch_assoc();
                                                                ?>
                                                                    <span class="badge badge-danger ml-1 mt-1"><?= $nomCategoriaF['nombre'] ?></span>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <div class="row mt-2">
                                                            <div class="col-md-12">
                                                                <b> <a href="perfil.php?id_user=<?= $row['rut_user'] ?>"><?php echo $nombreF['nombre'] ?></a></b>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <span class="badge badge-info"><?= $row["fecha"] ?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <!-----POST ------>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <div class="col-12 mt-2">
                                            <h1 class="text-center">No hay publicaciones por ahora :(</h1>
                                            <h2 class="text-center">Se el primero en agregar una!</h2>
                                        </div>
                                    <?php    } ?>
                                </div>
                            </div>

                            <div class="filtrados">

                            </div>

                            <div class="cargando" style="display: none;">
                                <div class="row">
                                    <div class="col-12 text-center mt-2">
                                        <img src="img/loading.gif" alt="Cargando">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <!-----/ FIIIIN    CONTENIDO -------------------------->
        <?php include("footer.php"); ?>
    </div>
    </div>


</body>

</html>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="js/scripts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
<script src="assets/demo/chart-area-demo.js"></script>
<script src="assets/demo/chart-bar-demo.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="assets/demo/datatables-demo.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="js/filtros2.js"></script>